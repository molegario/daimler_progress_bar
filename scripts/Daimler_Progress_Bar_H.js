// namespace all code so that we can control what seeps into the window object
(function (x2o, window) {
    var _this_;     // store the this pointer at instantiation, to resolve it correctly in callbacks
    $(function () { bootstrap(); });

    function x2oThermometer() {
        _this_ = this;
        var _subscribed,
            _updateInterval = 5,
            _updateIntervalId,
            //_lastData,
            _ready = false,
            _$main,
            _chart,
            _canvas,
            _context,
            _nextValue,
            _currentValue,
            _animation_loop,
            _color = ['Gradient(white;red)'];

        //Object does not support configuring a title bar or the background style.
        _this_.setInitOptions({
            showHeader: 'disabled',
            backgroundStyle: 'disabled'
        });

        // this event is called when the defined properties have been updated (either by Designer
        // or by Xynco at run-time.
        this.onPropertiesUpdated = function () {
            // TODO: implement all object changes on property value updates here.
            var resolvedColors = x2o.ColorClasses.getResolvedColorsList(_this_.properties_.values['Colors']);
            _color = ['Gradient(' + resolvedColors.join(':') + ')'];
            //Gradient(#c00:red:#f66:#fcc)

            _ready = false;
            _this_.drawVisualization();
            clearInterval(_updateIntervalId);
            if (_updateInterval != 0) {
                _updateIntervalId = setInterval(_this_.drawVisualization, _updateInterval * 1000);
            }
        };

        this.onStylesheetLoaded = function () {
            this.onPropertiesUpdated();
        };

        this.init = function () {
            var done = $.Deferred();

            _canvas = document.getElementById('cvs');
            _context = _canvas.getContext('2d');
            _$main = $("#MainDiv");

            $.when(this.construct(
                 window.name,
                 [
                     {
                         'name': "Value",
                         'type': "String",
                         'description': "The bar value",
                         'extendedDescription': "The progress bar extracts the value from a data source set up in a Data Access object. Enter the column name from your data source directly, or drag a column header from the Data Viewer table and drop it in the Value section of the bar. The object reads records from the data source and updates the bar value.",
                         'value': 'random'
                     },
                     //{
                     //    'name': "Label",
                     //    'type': "String",
                     //    'description': "The text for the title",
                     //    'value': ""
                     //},
                     {
                         'name': "Min",
                         'type': "Integer",
                         'description': "The minimum value of the bar",
                         'extendedDescription': "The minimum value that the scale displays. This is the number in which the scale starts. The default is zero (0).",
                         'value': "0"
                     },
                     {
                         'name': "Max",
                         'type': "Integer",
                         'description': "The maximum value of the bar",
                         'extendedDescription': "The maximum value that the scale displays. This is the number in which the scale ends. The default is 100.",
                         'value': "100"
                     },
                     {
                         'name': "Decimals",
                         'type': "Integer",
                         'description': "The number of decimal places",
                         'extendedDescription': "The number of decimal places in which numbers displayed in the scale are formatted.<br><br>1 = Tenths<br>2 = Hundredths<br>3 = Thousandths<br>4 = Ten-thousandths<br><br>Leave the value of this property blank to display ordinary whole numbers.",
                         'value': "1"
                     },
                     {
                        'name': "IncrementOverride",
                        'type': "Integer",
                        'description': "The total number of scale increments",
                        'extendedDescription': "The number of displayed increments on the horizontal Axis",
                        'value': "-1"
                    },
                    {
                         'name': "Colors",
                         'type': "ColorList",
                         'description': "The gradient colors inside the bar",
                         'extendedDescription': "Select the start and end colors that form a linear gradient inside the progress bar from the color picker, or enter your own color values separated by a semi-colon. Select one color to display a solid fill.",
                         'value': "white;red"
                     },
                     {
                         'name': "Margin",
                         'type': "Integer",
                         'description': "The margin between the bar and border",
                         'extendedDescription': "The number of pixels between the color indicator and the top and bottom edges of the bar.",
                         'value': "10"
                     },
                 ]
            )).then(function () {
                _this_.fire_onObjectLoad();
                _this_.notifyObjectReady();
                done.resolve();
            });

            return done.promise();
        };

        this.onWindowResize = function () {
            $('#cvs').attr('width', getBrowserWidth()).attr('height', getBrowserHeight());
            if (_ready) {
                _ready = false;
                _this_.drawVisualization();
            }
        };

        this.getDataArray = function () {
 
            if (_this_.properties_.values['Value'] == 'random') {
                return randomNumber(_this_.properties_.values['Min'] * 1, _this_.properties_.values['Max'] * 1);
            }
            else if (_this_.properties_.values['Value'].indexOf('$') == -1) {
                return _this_.properties_.values['Value'] * 1;
            }
            else {
                return _this_.getData();
            }
            
        };

        this.getData = function (setSource) {
            var value = 0;

            try {
                var dataSource = _this_.properties_.values['Value'];
                var objectName = getTagContents(dataSource, "$", ".");
                value = eval(dataSource.replace('$' + objectName, 'getAnyObjectByName("' + objectName + '")'));
            } catch (er) { /*console.log(er);*/ }

            return value;
        };

        this.drawVisualization = function (value, animating) {
            var _increment = _this_.properties_.values['IncrementOverride'] > 0 ? _this_.properties_.values['IncrementOverride'] * 1 : 10;
            value = (value || _this_.getDataArray() )*1;

            //Should use setSelection() the 2nd time
            if (!_ready || animating) {
                _ready = true;
                _currentValue = value;
                _context.clearRect(0, 0, _canvas.width, _canvas.height);                
                _chart = new RGraph.HProgress('cvs', _this_.properties_.values['Min'] * 1, _this_.properties_.values['Max'] * 1, value)
                                .Set('scale.decimals', _this_.properties_.values['Decimals'] * 1)
                                .Set('chart.labels.count', _increment)
                                .Set('gutter.left', 40)
                                .Set('gutter.right', 40)
                                .Set('colors', _color)
                                .Set('margin', _this_.properties_.values['Margin'] * 1)
                                .Set('tickmarks.inner', true)
                                .Draw();
            } else {
                _nextValue = value;
                var difference = _nextValue - _currentValue;

                animate_to;
                if (difference !== 0) {
                    clearInterval(_animation_loop);
                    _animation_loop = setInterval(animate_to, Math.floor(100 / Math.abs(difference)));
                }
            }
            
        };

        //function to make the chart move to new degrees
        function animate_to() {
            //clear animation loop if degrees reaches to new_degrees
            if (_nextValue == _currentValue) {
                clearInterval(_animation_loop);
                return;
            }

            var diff = Math.floor(_nextValue - _currentValue);

            if (diff > 0) {
                _currentValue += 1;
            } else if (diff < 0) {
                _currentValue -= 1;
            } else {
                _currentValue = _nextValue;
            }

            //requestAnimationFrame(function () {
            _this_.drawVisualization(_currentValue, true);
            //});
        }

        this.setDataSource = function (newDataSource, property) {
            _this_.properties_.values['UpdateInterval'] = 0;
            //if (property == 'Label') {
            //    _this_.properties_.values['Label'] = newDataSource;
            //} else {
                _this_.properties_.values['Value'] = newDataSource;
            //}
            _this_.drawVisualization();
        };

        this.subscribeTo = function (dataProviderName) {
            try {
                if (dataProviderName && dataProviderName != _subscribed) {
                    getAnyObjectByName(dataProviderName, function (obj) {
                        obj.addDataCallback(_this_.drawVisualization);
                        _updateInterval = 0;
                        clearInterval(_updateIntervalId);
                        _subscribed = dataProviderName;
                        _this_.drawVisualization();
                    });
                }
            } catch (err) {
                setTimeout(function () { _this_.subscribeTo(dataProviderName); }, 500);
            }
        };

        this.connectToData = function () {
            //External Data//////////////////////////////////
            var dataName = getTagContents(_this_.properties_.values['Value'], "$", ".");
            if (dataName) {
                _this_.subscribeTo(dataName);
            }
            /////////////////////////////////////////////////
        }

        /* class constructor code */
        this.onWindowResize();          // force initial sizing of elements       
    };

    


    // Inheritance setup
    x2oThermometer.inheritsFrom(x2o.ObjectBase);

    // Export object from namespace 
    window.x2oThermometer = new x2oThermometer();
    function bootstrap() {
        $.when(window.x2oThermometer.init()).then(function () {
            // POST-CONSTRUCTION CODE
            _this_.onWindowResize();
            _this_.connectToData();

            x2o.DataObjects.Consumer.construct({
                dropProperties: [/*{ property: 'Label', description: 'Title' },*/ { property: 'Value', description: 'Value' }],
                isInDesignMode: _this_.isInDesignMode(),
            });
        });
    };
}(x2o, window));




///NOW for the messy part


//DATA INTERFACE /////////////////////////////////
function setDataSource(newDataSource, property) {
    window.x2oThermometer.setDataSource(newDataSource, property);
}

function tickNow() {
    window.x2oThermometer.drawVisualization();
}

function subscribeTo(dataProviderName) {
    window.x2oThermometer.subscribeTo(dataProviderName);
}